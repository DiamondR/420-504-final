package ca.claurendeau.examen504final;

/**
 * 
 * Remanier le code ci-dessous de façon à ce qu'il utilise le 'State/Strategy Pattern'
 * 
 * Vous devrez nommer chacun des remaniements que vous faites ainsi que de faire un commit à chaque remaniement.
 *
 */
public class Personne {
    
    public final static String HEUREUSE = "heureuse";
    public final static String MALHEUREUSE = "malheureuse";
    public final static String TRISTE = "triste";
    
    private String humeur;

    public Personne(String humeur) {
        this.humeur = humeur;
    }

    @Override
    public String toString() {
        return "Personne [humeur=" + humeur + "]";
    }

    public String getHumeur() {
        return humeur;
    }
    
}
